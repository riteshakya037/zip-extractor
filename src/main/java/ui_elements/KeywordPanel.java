package ui_elements;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;

/**
 * Created by Ritesh Shakya on 6/17/2016.
 */
public class KeywordPanel extends JPanel {
    private JCheckBox classCheck;
    private JLabel keywordLbl;
    private JCheckBox checkDomainBox;
    private JCheckBox checkEmailBox;
    private int preferredHeight = 32;


    KeywordPanel(String keyword, boolean checkDomain, boolean checkEmail) {
        setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
        setName(keyword);
        classCheck = makeCheckbox(false, 40);
        keywordLbl = makeLabel(keyword, 140);
        checkDomainBox = makeCheckbox(checkDomain, 50);
        checkEmailBox = makeCheckbox(checkEmail, 20);

        setSize(new Dimension(300, preferredHeight));
        setBorder(new EtchedBorder());
        setVisible(true);
    }

    private JCheckBox makeCheckbox(boolean checkDomain, int width) {
        JCheckBox chk = new JCheckBox();
        chk.setSelected(checkDomain);
        chk.setPreferredSize(new Dimension(width, preferredHeight));
        add(chk);
        return chk;
    }

    private JLabel makeLabel(String text, int width) {
        JLabel t = new JLabel(text);
        t.setPreferredSize(new Dimension(width, preferredHeight));
        add(t);
        t.setHorizontalAlignment(SwingConstants.LEFT);
        return t;
    }

    boolean getSelected() {
        return classCheck.isSelected();
    }

    void setSelected(boolean select) {
        classCheck.setSelected(select);
    }

    public String getKeyword() {
        return keywordLbl.getText();
    }

    public boolean getCheckDomain() {
        return checkDomainBox.isSelected();
    }

    public boolean getCheckEmail() {
        return checkEmailBox.isSelected();
    }

    void setCheckDomain(boolean chk) {
        checkDomainBox.setSelected(chk);
    }

    void setCheckEmail(boolean chk) {
        checkEmailBox.setSelected(chk);
    }
}
