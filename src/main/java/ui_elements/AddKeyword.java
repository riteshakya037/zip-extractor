package ui_elements;

import utility_and_helper.KeywordWrapper;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;

public class AddKeyword extends JDialog {
    private JPanel contentPane;
    private JButton buttonAdd;
    private JButton buttonCancel;
    private JTextField keywordTxt;
    private JComboBox<ComboItem> domainCombo;
    private JComboBox<ComboItem> emailCombo;
    private HashMap<String, KeywordWrapper> keywordHashMap;
    protected boolean add = false;

    AddKeyword(HashMap<String, KeywordWrapper> keywordHashMap) {
        this.keywordHashMap = keywordHashMap;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonAdd);

        domainCombo.addItem(new ComboItem("True", true));
        domainCombo.addItem(new ComboItem("False", false));

        emailCombo.addItem(new ComboItem("True", true));
        emailCombo.addItem(new ComboItem("False", false));


        buttonAdd.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        keywordTxt.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                warn();
            }

            public void removeUpdate(DocumentEvent e) {
                warn();
            }

            public void insertUpdate(DocumentEvent e) {
                warn();
            }

            void warn() {
                if (keywordTxt.getText().trim().length() != 0) {
                    if (!keywordTxt.getText().trim().matches("[\\w]+")) {
                        JOptionPane.showMessageDialog(null,
                                "Error Please enter Valid Data", "Error Massage",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        buttonAdd.setEnabled(true);
                    }
                } else {
                    buttonAdd.setEnabled(false);
                }
            }
        });
        setSize(new Dimension(320, 180));
        setLocationRelativeTo(null);
        setVisible(true);
    }


    String getKeyWord() {
        return keywordTxt.getText();
    }

    boolean getDomainCheck() {
        return Boolean.parseBoolean(domainCombo.getItemAt(domainCombo.getSelectedIndex()).getValue());
    }

    boolean getEmailCheck() {
        return Boolean.parseBoolean(domainCombo.getItemAt(emailCombo.getSelectedIndex()).getValue());
    }

    private void onOK() {
        if (!keywordHashMap.containsKey(getKeyWord())) {
            keywordHashMap.put(getKeyWord(), new KeywordWrapper(getDomainCheck(), getEmailCheck()));
            setVisible(false);
            add = true;
        } else {
            JOptionPane.showMessageDialog(null,
                    "Keyword already present", "Error Massage",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    private void onCancel() {
        dispose();
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    class ComboItem {
        private String key;
        private String value;

        ComboItem(String key, boolean value) {
            this.key = key;
            this.value = String.valueOf(value);
        }

        @Override
        public String toString() {
            return key;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }
}
