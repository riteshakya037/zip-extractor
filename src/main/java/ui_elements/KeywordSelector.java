package ui_elements;

import constants.Constants;
import utility_and_helper.FileUtils;
import utility_and_helper.KeywordWrapper;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class KeywordSelector extends JDialog {
    private JPanel contentPane;
    private JButton buttonAdd;
    private JButton buttonSelectAll;
    private JButton buttonDeselect;
    private JButton buttonRemove;
    private JPanel insideScrollPanel;
    private JButton importCsvButton;
    private JLabel countLabel;
    private JLabel importLabel;
    HashMap<String, KeywordWrapper> keywordHashMap = new HashMap<>();

    KeywordSelector() {
        setContentPane(contentPane);
        setModal(true);
        setTitle("KeyWord Selector");
        getRootPane().setDefaultButton(buttonAdd);
        keywordHashMap = FileUtils.readKeywords();
        createKeywords();
        buttonAdd.addActionListener(e -> onAdd());
        buttonSelectAll.addActionListener(e -> onSelectAll());
        buttonDeselect.addActionListener(e -> onDeselectAll());
        buttonRemove.addActionListener(e -> onDelete());
        importCsvButton.addActionListener(e -> importKeyword());


        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onClose();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onClose(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        setSize(new Dimension(340, 400));
        setLocationRelativeTo(null);

        setResizable(false);
        setVisible(true);
    }

    private void importKeyword() {
        JFileChooser chooser = new JFileChooser(new File(Constants.INPUT_SELECTOR_LOCATION));
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files", "txt", "text", "csv");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Select Keyword Csv");
        // disable the "All files" option.
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            appendKeyword(FileUtils.importKeyWord(chooser.getSelectedFile()));
        }
    }

    private void appendKeyword(HashMap<String, KeywordWrapper> hashMap) {
        int count = 0;
        for (Map.Entry<String, KeywordWrapper> singleRow : hashMap.entrySet()) {

            if (keywordHashMap.containsKey(singleRow.getKey())) {
                keywordHashMap.replace(singleRow.getKey(), singleRow.getValue());
                updateKeyword(singleRow.getKey(), singleRow.getValue().domain, singleRow.getValue().email);
            } else {
                count++;
                keywordHashMap.put(singleRow.getKey(), singleRow.getValue());
                addKeyword(singleRow.getKey(), singleRow.getValue().domain, singleRow.getValue().email);
            }
        }
        importLabel.setText("Total new Keyword Imported: " + count);
    }

    private void addKeyword(String keyword, boolean domain, boolean email) {
        insideScrollPanel.add(new KeywordPanel(keyword, domain, email));
        insideScrollPanel.revalidate();
        insideScrollPanel.repaint();
        updateCount();
    }

    private void updateCount() {
        countLabel.setText("Total Keywords: " + insideScrollPanel.getComponents().length);
    }

    private void updateKeyword(String key, boolean domain, boolean email) {
        for (Component eachRow : insideScrollPanel.getComponents()) {
            if (eachRow.getName().equalsIgnoreCase(key)) {
                KeywordPanel keywordPanel = (KeywordPanel) eachRow;
                keywordPanel.setCheckDomain(domain);
                keywordPanel.setCheckEmail(email);
            }
        }
    }

    private void createKeywords() {
        insideScrollPanel.setLayout(new GridLayout(0, 1));
        for (Map.Entry<String, KeywordWrapper> keySet : keywordHashMap.entrySet()) {
            addKeyword(keySet.getKey(), keySet.getValue().domain, keySet.getValue().email);
        }
    }

    private void onDeselectAll() {
        for (Component component : insideScrollPanel.getComponents()) {
            KeywordPanel keywordPanel = (KeywordPanel) component;
            keywordPanel.setSelected(false);
        }
    }

    private void onSelectAll() {
        for (Component component : insideScrollPanel.getComponents()) {
            KeywordPanel keywordPanel = (KeywordPanel) component;
            keywordPanel.setSelected(true);
        }
    }

    private void onDelete() {
        for (Component component : insideScrollPanel.getComponents()) {
            KeywordPanel keywordPanel = (KeywordPanel) component;
            if (keywordPanel.getSelected()) {
                keywordHashMap.remove(keywordPanel.getKeyword());
                insideScrollPanel.remove(component);
                insideScrollPanel.revalidate();
                insideScrollPanel.repaint();
            }
        }
    }

    private void onAdd() {
        AddKeyword addKeyword = new AddKeyword(keywordHashMap);
        if (addKeyword.add) {
            addKeyword(addKeyword.getKeyWord(), addKeyword.getDomainCheck(), addKeyword.getEmailCheck());
        }
    }

    private void onClose() {
        int option = JOptionPane.showConfirmDialog(null, "Save Changes", "Close", JOptionPane.YES_NO_OPTION);

        if (option == JOptionPane.YES_OPTION) { //The ISSUE is here
            FileUtils.writeKeyWord(insideScrollPanel.getComponents());
            dispose();
        } else {
            dispose();
        }

    }


    public static void main(String[] args) {
        new KeywordSelector();
        System.exit(0);
    }

}
