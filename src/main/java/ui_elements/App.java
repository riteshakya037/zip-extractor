package ui_elements;

import constants.AppProperties;
import constants.Constants;
import file_elements.FileListGetter;
import utility_and_helper.FileUtils;
import utility_and_helper.StringUtils;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ritesh Shakya on 6/1/2016.
 */
public class App extends JFrame implements ActionListener {
    private static boolean previousRun = false;
    private static long timeRun;
    private final GridBagConstraints constraints;
    private static JTextField inputDirectoryText;
    private static JLabel programStatusLabel;
    private static JLabel statusLabel;
    private static JPanel mainPanel;
    private static JPanel checkBoxPanel;
    private static JCheckBox processZip;
    private JButton selectFolderButton, startButton, keywordButton;
    private final JPopupMenu menuPopup = new JPopupMenu();

    private App() {
        super(FileUtils.getProjectProperties("name") + " " + FileUtils.getProjectProperties("version"));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        //Make text boxes
        getContentPane().setLayout(new BorderLayout());
        constraints = new GridBagConstraints();
        constraints.insets = new Insets(3, 10, 3, 0);
        createGUI();
        if (StringUtils.isNull(inputDirectoryText.getText())) {
            startButton.setEnabled(false);
        }
        if (previousRun) {
            NumberFormat formatter = new DecimalFormat("#0.00");
            statusText("Extraction Completed in " + formatter.format((timeRun) / 1000d) + " seconds", "");
        }
        //Display the window.
        pack();
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }

    public static String getJarPath() {
        try {
            return URLDecoder.decode(App.class.getProtectionDomain().getCodeSource().getLocation().getPath(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        //Setting UI as default
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        //Start UI Thread
        if (args.length != 0) {
            previousRun = true;
            timeRun = Long.parseLong(args[0]);
        }
        SwingUtilities.invokeLater(App::new);
    }

    private static void setPanelEnabled(JPanel panel, Boolean isEnabled) {
        panel.setEnabled(isEnabled);

        Component[] components = panel.getComponents();

        for (Component component : components) {
            if (component.getClass().getName().equals("javax.swing.JPanel")) {
                setPanelEnabled((JPanel) component, isEnabled);
            }
            component.setEnabled(isEnabled);
        }
    }

    private JTextField makeText(JPanel mainPanel) {
        JTextField t = new JTextField(FileUtils.getProperty(AppProperties.InputLocation), 40);
        t.setEditable(false);
        t.setHorizontalAlignment(JTextField.LEFT);
        t.setBackground(Color.WHITE);
        mainPanel.add(t, constraints);
        return t;
    }

    private JLabel makeLabel(String text, JPanel mainPanel, Object layout, int alignment) {
        JLabel t = new JLabel(text);
        t.setHorizontalAlignment(alignment);
        t.setBorder(new EmptyBorder(0, 5, 0, 5));
        mainPanel.add(t, layout);
        return t;
    }

    private JButton makeButton(String caption, JPanel mainPanel, String layout) {
        JButton b = new JButton(caption);
        b.setSize(new Dimension(110, 24));
        b.setActionCommand(caption);
        b.addActionListener(this);
        mainPanel.add(b, layout);
        return b;
    }

    private void createGUI() {

        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        mainPanel.setBorder(new EmptyBorder(5, 0, 0, 0));

        makeLabel("Input Directory", mainPanel, constraints, SwingConstants.LEFT);
        inputDirectoryText = makeText(mainPanel);
        //Make buttons
        selectFolderButton = makeButton("Select Folder", mainPanel);
        getRootPane().setDefaultButton(selectFolderButton);
        startButton = makeButton("Start Extraction", mainPanel);

        JPanel statusPanel = new JPanel();
        statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
        statusPanel.setPreferredSize(new Dimension(getContentPane().getWidth(), 16));
        statusPanel.setLayout(new BorderLayout());
        programStatusLabel = makeLabel("Initialization Complete", statusPanel, BorderLayout.LINE_START, SwingConstants.LEFT);
        statusLabel = makeLabel("", statusPanel, BorderLayout.CENTER, SwingConstants.RIGHT);

        JMenuItem cancelMenu = new JMenuItem("Cancel");
        menuPopup.add(cancelMenu);
        statusPanel.setComponentPopupMenu(menuPopup);

        checkBoxPanel = new JPanel();
        checkBoxPanel.setLayout(new BorderLayout());
        JPanel innerPanel = new JPanel();
        innerPanel.setLayout(new FlowLayout());
        checkBoxPanel.add(innerPanel, BorderLayout.EAST);
        processZip = new JCheckBox("Process Zip");
        processZip.setSelected(false);
        processZip.setPreferredSize(new Dimension(110, 24));

        keywordButton = new JButton("Select Keyword");
        keywordButton.setPreferredSize(new Dimension(110, 24));
        keywordButton.addActionListener(this);
        innerPanel.add(processZip);
        innerPanel.add(keywordButton);

        getContentPane().add(mainPanel, BorderLayout.NORTH);
        getContentPane().add(checkBoxPanel, BorderLayout.CENTER);
        getContentPane().add(statusPanel, BorderLayout.SOUTH);
    }

    private JButton makeButton(String caption, JPanel mainPanel) {
        JButton b = new JButton(caption);
        b.setPreferredSize(new Dimension(110, 24));
        b.setActionCommand(caption);
        b.addActionListener(this);
        mainPanel.add(b, constraints);
        return b;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == selectFolderButton) {
            JFileChooser chooser = new JFileChooser(new File(Constants.INPUT_SELECTOR_LOCATION));
            chooser.setDialogTitle("Select Input Folder");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            // disable the "All files" option.
            chooser.setAcceptAllFileFilterUsed(false);
            if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                startButton.setEnabled(true);
                inputDirectoryText.setText(String.valueOf(chooser.getSelectedFile()));
            }
        } else if (e.getSource() == startButton) {
            FileUtils.saveProperty(AppProperties.InputLocation, inputDirectoryText.getText());
            setPanelEnabled(mainPanel, false);
            setPanelEnabled(checkBoxPanel, false);
            try {
                String logOutput = inputDirectoryText.getText() + Constants.LOG_DIRECTORY;
                if (!(new File(logOutput)).exists()) {
                    (new File(logOutput)).mkdir();
                }
                System.setOut(new PrintStream(new BufferedOutputStream(new FileOutputStream(logOutput + new SimpleDateFormat("'log-'yyyy-MM-dd hh-mm-ss'.tsv'").format(new Date()))), true));
                (new FileListGetter(inputDirectoryText.getText())).execute();
            } catch (FileNotFoundException e1) {
                statusText("Input Location not Found", "");
                reEnableUI();
            }
        } else if (e.getSource() == keywordButton) {
            new KeywordSelector();
        }
    }

    public static void reEnableUI() {
        System.out.close();
        setPanelEnabled(mainPanel, true);
        setPanelEnabled(checkBoxPanel, true);
    }

    public static void statusText(String programStatusText, String fileStatusText) {
        if (programStatusText != null)
            programStatusLabel.setText(programStatusText);
        if (fileStatusText != null)
            statusLabel.setText(fileStatusText);
    }

    public static boolean isZipProcessSelected() {
        return processZip.isSelected();
    }

    public static String getInputDirectory() {
        return inputDirectoryText.getText();
    }
}
