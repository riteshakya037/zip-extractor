package xls_parser_elements;

/**
 * Enumeration used to tell the CSVParser what to consider null.
 * <p/>
 * EMPTY_SEPARATORS - two sequential separators are null.
 * EMPTY_QUOTES - two sequential quotes are null
 * BOTH - both are null
 * NEITHER - default.  Both are considered empty string.
 * <p>
 * Created by Ritesh Shakya on 6/11/2016.
 */

public enum CSVReaderNullFieldIndicator {
    EMPTY_SEPARATORS,
    EMPTY_QUOTES,
    BOTH,
    NEITHER
}