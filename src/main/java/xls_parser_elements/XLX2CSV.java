package xls_parser_elements;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import utility_and_helper.FileAndExtensionWrapper;
import utility_and_helper.FileUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Iterator;

/**
 * Created by Ritesh Shakya on 6/1/2016.
 */
class XLX2CSV {

    XLX2CSV(FileAndExtensionWrapper inputFile) throws IOException {

        try {
            long start = System.currentTimeMillis();

            HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(inputFile.filePath));
            FileOutputStream fos = new FileOutputStream(FileUtils.createTempOutput(inputFile.filePath));

            Sheet sheet = workbook.getSheetAt(0);
            for (Row aSheet : sheet) {
                fos.write(sheetIterator(aSheet).getBytes());
            }
            fos.close();
            long end = System.currentTimeMillis();
            NumberFormat formatter = new DecimalFormat("#0.00");
            System.out.println("Conversion to CSV for " + FileUtils.getFile(inputFile.filePath) + " completed  " + formatter.format((end - start) / 1000d) + " seconds");
        } catch (FileNotFoundException e) {
            e.printStackTrace(System.out);
        }
    }

    private String sheetIterator(Row row) {
        StringBuilder cellDData = new StringBuilder();
        Cell cell;

        Iterator<Cell> cellIterator = row.cellIterator();
        while (cellIterator.hasNext()) {
            cell = cellIterator.next();

            switch (cell.getCellType()) {

                case Cell.CELL_TYPE_BOOLEAN:
                    cellDData.append(cell.getBooleanCellValue()).append(",");
                    break;

                case Cell.CELL_TYPE_NUMERIC:
                    cellDData.append(cell.getNumericCellValue()).append(",");
                    break;

                case Cell.CELL_TYPE_STRING:
                    if (cell.getStringCellValue().contains(","))
                        cellDData.append("\"").append(cell.getStringCellValue().replaceAll("\"", "")).append("\"").append(",");
                    else
                        cellDData.append(cell.getStringCellValue()).append(",");
                    break;

                case Cell.CELL_TYPE_BLANK:
                    cellDData.append("" + ",");
                    break;

                default:
                    cellDData.append(cell.toString().replaceAll("\"", "")).append(",");
            }
        }
        return cellDData.append(System.lineSeparator()).toString();
    }

}