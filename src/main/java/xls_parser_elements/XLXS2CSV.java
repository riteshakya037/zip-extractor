package xls_parser_elements;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.SAXHelper;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler.SheetContentsHandler;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import utility_and_helper.FileAndExtensionWrapper;
import utility_and_helper.FileUtils;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import static java.nio.file.StandardOpenOption.CREATE;

/**
 * Created by Ritesh Shakya on 6/1/2016.
 */
class XLXS2CSV {
    private final OPCPackage xlsxPackage;
    /**
     * Number of columns to read starting with leftmost
     */
    private final int minColumns;
    /**
     * Destination for data
     */
    private final PrintStream output;

    XLXS2CSV(FileAndExtensionWrapper inputFile) throws OpenXML4JException, ParserConfigurationException, SAXException, IOException {
        long start = System.currentTimeMillis();

        File xlsxFile = new File(inputFile.filePath);
        xlsxPackage = OPCPackage.open(xlsxFile.getPath(), PackageAccess.READ);
        output = new PrintStream(Files.newOutputStream(Paths.get(FileUtils.createTempOutput(inputFile.filePath)), CREATE));
        minColumns = -1;
        process();
        xlsxPackage.close();
        long end = System.currentTimeMillis();
        NumberFormat formatter = new DecimalFormat("#0.00");
        System.out.println("Conversion to CSV for " + FileUtils.getFile(inputFile.filePath) + " completed  " + formatter.format((end - start) / 1000d) + " seconds");

    }

    /**
     * Parses and shows the content of one sheet
     * using the specified styles and shared-strings tables.
     *
     * @param styles           StylesTable
     * @param strings          ReadOnlySharedStringsTable
     * @param sheetInputStream InputStream for sheet
     */
    private void processSheet(
            StylesTable styles,
            ReadOnlySharedStringsTable strings,
            SheetContentsHandler sheetHandler,
            InputStream sheetInputStream)
            throws IOException, SAXException {
        DataFormatter formatter = new DataFormatter();
        InputSource sheetSource = new InputSource(sheetInputStream);
        try {
            XMLReader sheetParser = SAXHelper.newXMLReader();
            ContentHandler handler = new XSSFSheetXMLHandler(
                    styles, null, strings, sheetHandler, formatter, false);
            sheetParser.setContentHandler(handler);
            sheetParser.parse(sheetSource);
        } catch (ParserConfigurationException e) {
            e.printStackTrace(System.out);
        }
    }

    /**
     * Initiates the processing of the XLS workbook file to CSV.
     *
     * @throws IOException
     * @throws OpenXML4JException
     * @throws ParserConfigurationException
     * @throws SAXException
     */
    private void process()
            throws IOException, OpenXML4JException, ParserConfigurationException, SAXException {
        ReadOnlySharedStringsTable strings = new ReadOnlySharedStringsTable(this.xlsxPackage);
        XSSFReader xssfReader = new XSSFReader(this.xlsxPackage);
        StylesTable styles = xssfReader.getStylesTable();
        XSSFReader.SheetIterator sheetIterator = (XSSFReader.SheetIterator) xssfReader.getSheetsData();
        while (sheetIterator.hasNext()) {
            InputStream stream = sheetIterator.next();
            processSheet(styles, strings, new SheetToCSV(), stream);
            stream.close();
        }
    }

    /**
     * Uses the XSSF Event SAX helpers to do most of the work
     * of parsing the Sheet XML, and outputs the contents
     * as a (basic) CSV.
     */
    private class SheetToCSV implements SheetContentsHandler {
        private boolean firstCellOfRow = false;
        private int currentRow = -1;
        private int currentCol = -1;

        private void outputMissingRows(int number) {
            for (int i = 0; i < number; i++) {
                for (int j = 0; j < minColumns; j++) {
                    output.append(',');
                }
                output.append('\n');
            }
        }

        public void startRow(int rowNum) {
            outputMissingRows(rowNum - currentRow - 1);
            firstCellOfRow = true;
            currentRow = rowNum;
            currentCol = -1;
        }

        public void endRow(int rowNum) {
            for (int i = currentCol; i < minColumns; i++) {
                output.append(',');
            }
            output.append('\n');
        }

        public void cell(String cellReference, String formattedValue,
                         XSSFComment comment) {
            if (firstCellOfRow) {
                firstCellOfRow = false;
            } else {
                output.append(',');
            }

            if (cellReference == null) {
                cellReference = new CellAddress(currentRow, currentCol).formatAsString();
            }

            int thisCol = (new CellReference(cellReference)).getCol();
            int missedCols = thisCol - currentCol - 1;
            for (int i = 0; i < missedCols; i++) {
                output.append(',');
            }
            currentCol = thisCol;
            try {
                Double.parseDouble(formattedValue);
                output.append(formattedValue.replaceAll("\"", ""));
            } catch (NumberFormatException e) {
                if (formattedValue.contains(",")) {
                    output.append('"');
                    output.append(formattedValue.replaceAll("\"", ""));
                    output.append('"');
                } else {
                    output.append(formattedValue.replaceAll("\"", ""));
                }
            }
        }

        public void headerFooter(String text, boolean isHeader, String tagName) {
        }
    }
}