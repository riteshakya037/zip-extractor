package xls_parser_elements;

import constants.Constants;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.xml.sax.SAXException;
import utility_and_helper.FileAndExtensionWrapper;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Gateway class for Parsing Excel files
 * Created by Ritesh Shakya on 6/6/2016.
 */
public class ExcelParser {

    /**
     * Input File
     *
     * @see FileAndExtensionWrapper
     */
    private FileAndExtensionWrapper inputFile;

    /**
     * @param inputFile Input File
     * @throws Exception
     */
    public void generateCSV(FileAndExtensionWrapper inputFile) throws Exception {
        this.inputFile = inputFile;
        prepareReader();
    }

    /**
     * Checks extension of {@link #inputFile} and processes accordingly
     *
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws OpenXML4JException
     * @see XLX2CSV for XLS Files
     * @see XLXS2CSV for XLSX Files
     */
    private void prepareReader() throws IOException, ParserConfigurationException, SAXException, OpenXML4JException {
        String fileFormat = inputFile.extension;
        if (fileFormat.equalsIgnoreCase(Constants.XLSX)) {
            new XLXS2CSV(inputFile);
        } else {
            new XLX2CSV(inputFile);
        }
    }
}
