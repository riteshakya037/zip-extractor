package utility_and_helper;

/**
 * Wrapper Class to wrap selection specification for certain keyword
 * Created by Ritesh Shakya on 6/17/2016.
 */
public class KeywordWrapper {
    /**
     * Condition for keyword to contain with Domain Column
     */
    public boolean domain;
    /**
     * Condition for keyword to contain with Email Column
     */
    public boolean email;

    /**
     * @param checkDomain Check for keyword in Domain
     * @param checkEmail  Check for keyword in Email
     */
    public KeywordWrapper(String checkDomain, String checkEmail) {
        domain = Boolean.valueOf(checkDomain);
        email = Boolean.valueOf(checkEmail);
    }

    /**
     * @param domainCheck Check for keyword in Domain
     * @param emailCheck  Check for keyword in Email
     */
    public KeywordWrapper(boolean domainCheck, boolean emailCheck) {
        domain = domainCheck;
        email = emailCheck;
    }

    /**
     * @return @return {@link #domain} + {@link #email} for debugging purposes
     */
    @Override
    public String toString() {
        return domain + ", " + email;
    }
}
