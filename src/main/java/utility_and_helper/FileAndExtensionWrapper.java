package utility_and_helper;

import java.io.File;

/**
 * Wrapper class to wrap files {@link #filePath} and {@link #extension}
 * Created by Ritesh Shakya on 6/2/2016.
 */
public class FileAndExtensionWrapper {
    /**
     * Complete path of Input
     */
    public final String filePath;
    /**
     * Extension of Input File
     */
    public final String extension;

    public FileAndExtensionWrapper(File file) {
        filePath = file.getAbsolutePath();
        extension = getExtension(file.getName());
    }

    /**
     * @param fileName inputPath of the File
     * @return extension of the file
     */
    private String getExtension(String fileName) {
        String extension = "";
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i);
        }
        return extension;
    }

    /**
     * @return {@link #filePath} + {@link #extension} nicely separated by comma
     */
    @Override
    public String toString() {
        return filePath + ", " + extension;
    }
}
