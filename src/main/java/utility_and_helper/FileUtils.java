package utility_and_helper;

import constants.AppProperties;
import constants.Constants;
import ui_elements.KeywordPanel;
import ui_elements.KeywordSelector;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Properties;
import java.util.stream.Stream;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Utility class with many methods to help dealing with files
 * Created by Ritesh Shakya on 6/1/2016.
 */
public class FileUtils {

    /**
     * Location of Configuration File
     */
    private static final String configFile = System.getProperty("user.home") + File.separator + getProjectProperties("name") + File.separator + "config.properties";
    /**
     * Location of Keyword File
     */
    private static String keyWordFile = System.getProperty("user.home") + File.separator + getProjectProperties("name") + File.separator + "keyword.properties";
    /**
     * Separator used in {@link #keyWordFile} to separate columns
     */
    private static String hashSeparator = "::";


    /**
     * @param properties Enum of certain Program Configurations
     * @return String value of {@link AppProperties} as stored in {#link configFile}
     * @see AppProperties
     */
    public static String getProperty(AppProperties properties) {
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(configFile);
            prop.load(input);
            return prop.getProperty(properties.getValue());
        } catch (IOException ignored) {
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
    }

    /**
     * Reads the keywords stored in {@link #keyWordFile}
     *
     * @return HashMap containing keyword as key and {@link KeywordWrapper} as value containing boolean checkValues for domain and email
     * @see KeywordWrapper
     */
    public static HashMap<String, KeywordWrapper> readKeywords() {
        HashMap<String, KeywordWrapper> hash = new HashMap<>();
        Path file = Paths.get(keyWordFile);
        Path newDir = Paths.get(getFolder(keyWordFile));
        Stream<String> lines;
        try {
            if (!Files.exists(newDir))
                Files.createDirectories(newDir);
            lines = Files.lines(file, StandardCharsets.UTF_8);
            for (String line : (Iterable<String>) lines::iterator) {
                String[] lineMap = line.split(hashSeparator);
                hash.put(lineMap[0], new KeywordWrapper(lineMap[1], lineMap[2]));
            }
        } catch (IOException ignored) {
        }
        return hash;
    }

    /**
     * @param components All the components {@link KeywordPanel} contained in {@link JPanel} from {@link KeywordSelector}
     * @see KeywordPanel
     * @see JPanel#getComponents()
     * @see KeywordSelector
     */
    public static void writeKeyWord(Component[] components) {
        try (PrintWriter out = new PrintWriter(new FileWriter(keyWordFile))) {
            for (Component component : components) {
                KeywordPanel keywordPanel = (KeywordPanel) component;
                out.write(keywordPanel.getKeyword() + hashSeparator + keywordPanel.getCheckDomain() + hashSeparator + keywordPanel.getCheckEmail() + System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }

    /**
     * @param filePath Full path of the file
     * @return File Name
     */
    public static String getFile(String filePath) {
        return (new File(filePath)).getName();
    }

    /**
     * @param filePath Full path of the file
     * @return Parent Folder of filePath
     */
    public static String getFolder(String filePath) {
        return (new File(filePath)).getParent();
    }


    /**
     * @param filePath Full path of the file
     * @return Parent of Folder containing filePath
     */
    public static String getParent(String filePath) {
        return (new File((new File(filePath)).getParent())).getParent();
    }


    /**
     * @param filePath           Full path of the file
     * @param intermediateFolder Output Folder for file to copy into
     */
    public static void copyFile(String filePath, String intermediateFolder) {

        Path source = Paths.get(filePath);
        Path newDir = Paths.get(FileUtils.getFolder(filePath) + intermediateFolder);
        try {
            if (!Files.exists(newDir))
                Files.createDirectories(newDir);
            Files.copy(source, newDir.resolve(source.getFileName()), REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param filePath Full path of the file
     * @return Boolean of whether file was deleted
     */
    public static boolean delete(String filePath) {
        Path source = Paths.get(filePath);
        try {
            return Files.deleteIfExists(source);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Recursive deletion of everything inside a folder
     *
     * @param filePath Full path of the file
     */
    public static void deleteFolder(String filePath) {
        Path directory = Paths.get(filePath);
        try {
            Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }

            });
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }
    }


    /**
     * @param filePath  Full path of the file
     * @param condition Folder to move file into
     */
    public static void moveFile(String filePath, String condition) {

        Path source = Paths.get(filePath);
        Path newDir = Paths.get(FileUtils.getFolder(filePath) + condition);
        try {
            if (!Files.exists(newDir))
                Files.createDirectories(newDir);
            Files.move(source, newDir.resolve(source.getFileName()), REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * @param filePath Full path of the file
     * @return Temporary output for XLS and XLSX files after conversion to CSV
     */
    public static String createTempOutput(String filePath) {
        String inputFile = getFile(filePath);
        return getTempCSVLocation(filePath) + getCSVExtension(inputFile);
    }

    /**
     * @param inputFile Name of input file.
     * @return Name of output file with CSV as extension
     */
    private static String getCSVExtension(String inputFile) {
        return inputFile.substring(0, inputFile.lastIndexOf(".")) + Constants.CSV;
    }

    /**
     * @param filePath Full path of the file
     * @return Temporary folder to store CSV given by {@link Constants#TEMP_CSV_LOCATION}
     * @see Constants#TEMP_CSV_LOCATION
     */
    private static String getTempCSVLocation(String filePath) {
        if (!(new File(getFolder(filePath) + Constants.TEMP_CSV_LOCATION)).exists()) {
            (new File(getFolder(filePath) + Constants.TEMP_CSV_LOCATION)).mkdir();
        }
        return getFolder(filePath) + Constants.TEMP_CSV_LOCATION;
    }


    /**
     * @param properties Enum of certain Program Configurations
     * @param value      value to save to property defined by {@code properties}
     */
    public static void saveProperty(AppProperties properties, String value) {
        Properties prop = new Properties();
        File folder = new File(getFolder(configFile));
        if (!folder.exists()) {
            folder.mkdir();
        }
        OutputStream output = null;
        try {
            output = new FileOutputStream(configFile);
            prop.setProperty(properties.getValue(), value);
            prop.store(output, null);

        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    /**
     * Moves Folder along with all files inside it
     *
     * @param source Source Folder
     * @param target Destination Folder
     */
    public static void moveFolder(String source, String target) {
        Path sourcePath = Paths.get(source);
        Path targetPath = Paths.get(target);
        try {
            Files.move(
                    sourcePath,
                    targetPath,
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param property Project property name. e.g., version,name
     * @return value associated with the key defined by {@code property}
     */
    public static String getProjectProperties(String property) {
        final Properties properties = new Properties();
        try {
            properties.load(FileUtils.class.getResourceAsStream("/project.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(property);
    }

    /**
     * Import text file with keywords
     *
     * @param selectedFile Text File input location
     * @return HashMap containing keyword as key and {@link KeywordWrapper} as value containing boolean checkValues for domain and email
     * @see KeywordWrapper
     */
    public static HashMap<String, KeywordWrapper> importKeyWord(File selectedFile) {
        HashMap<String, KeywordWrapper> hashMap = new HashMap<>();

        try (Stream<String> lines = Files.lines(Paths.get(selectedFile.getPath()), StandardCharsets.UTF_8)) {
            for (String line : (Iterable<String>) lines::iterator) {
                String[] splitColumn = line.split(",");
                if (splitColumn.length == 3) {
                    hashMap.put(splitColumn[0], new KeywordWrapper("Y".equalsIgnoreCase(splitColumn[1]), "Y".equalsIgnoreCase(splitColumn[2])));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hashMap;
    }
}