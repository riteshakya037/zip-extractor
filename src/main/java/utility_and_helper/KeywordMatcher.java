package utility_and_helper;

import process_elements.KeyWordSync;
import process_elements.KeywordWriter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

/**
 * Matches certain columns with the desired keywords to know valid ones.
 * Created by Ritesh Shakya on 6/21/2016.
 */
public class KeywordMatcher {
    static HashMap<String, KeywordWrapper> keyHash = new HashMap<>();

    static {
        keyHash = FileUtils.readKeywords();
    }

    /**
     * @param email  Value form Email column
     * @param joiner Output row received from {@link KeywordWriter}
     * @see KeywordWriter
     */
    public static void matches(String email, StringJoiner joiner) {
        matches(null, email, joiner);
    }


    /**
     * @param domain Value form Domain column
     * @param email  Value form Email column
     * @param joiner Output row received from {@link KeywordWriter}
     * @see KeywordWriter
     */
    public static void matches(String domain, String email, StringJoiner joiner) {
        //write to keyword
        keyHash.entrySet().stream().filter(keywordSet -> matchEmail(email, keywordSet) || matchDomain(domain, keywordSet)).forEach(keywordSet -> {
            //write to keyword
            try {
                KeyWordSync.getInstance(keywordSet.getKey()).write(joiner);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * @param domain     Value form Domain column
     * @param keywordSet Set of Keyword with selection specification
     * @return Whether all condition for domain matches
     */
    private static boolean matchDomain(String domain, Map.Entry<String, KeywordWrapper> keywordSet) {
        return StringUtils.isNotNull(domain)
                && domain.contains(keywordSet.getKey())
                && keywordSet.getValue().domain;
    }

    /**
     * @param email      Value form Email column
     * @param keywordSet Set of Keyword with selection specification
     * @return Whether all condition for email matches
     */
    private static boolean matchEmail(String email, Map.Entry<String, KeywordWrapper> keywordSet) {
        return StringUtils.isNotNull(email)
                && email.contains(keywordSet.getKey())
                && keywordSet.getValue().email;
    }
}
