package utility_and_helper;

import process_elements.ProcessZipFiles;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * @Singleton class to sync write from thread
 * Created by Ritesh Shakya on 6/15/2016.
 */
public class SyncData {

    /**
     * Single instance of class
     */
    private static SyncData instance = null;
    /**
     * Set of zips and their respective counts stored as string in a order way
     *
     * @see TreeSet
     */
    public static final Set<String> zipCount = new TreeSet<>();

    /**
     * @return Single instance of class by initializing if not initialized
     * @throws IOException
     */
    public static SyncData getInstance() throws IOException {
        if (instance == null) {
            synchronized (SyncData.class) {
                instance = new SyncData();
            }
        }
        return instance;
    }

    /**
     * @param zip   zip number
     * @param count count of {@code zip} in each zip file
     */
    public void appendToHash(String zip, int count) {
        synchronized (instance) {
            zipCount.add(zip + "," + count);
            ProcessZipFiles.completed++;
        }
    }

}


