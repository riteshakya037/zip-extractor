package utility_and_helper;

/**
 * Wrapper class to wrap column name and position of every column in a row
 * Created by Ritesh Shakya on 6/7/2016.
 */
public class ColumnWrapper {
    /**
     * Name of Column
     */
    private final String name;
    /**
     * Position of this column in a Row
     */
    public final int position;

    /**
     * @param name     column name
     * @param position position of column in respect to starting 0
     */
    public ColumnWrapper(String name, int position) {
        this.name = name;
        this.position = position;
    }

    /**
     * @return {@link #name} + {@link #position} for debugging purposes
     */
    @Override
    public String toString() {
        return "ColumnWrapper{" +
                "name='" + name + '\'' +
                ", position=" + position +
                '}';
    }
}
