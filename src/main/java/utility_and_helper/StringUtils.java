package utility_and_helper;

import java.util.regex.Pattern;

/**
 * Created by Ritesh Shakya on 6/2/2016.
 */
public class StringUtils {

    /**
     * @param line Row of input
     * @return The maximum occurring character apart from {words, space, quotes("), hyphen(-)}
     */
    public static char getMaxOccurringChar(String line) {
        String pattern = "[^\\w\\s\"/\\.-]";
        char maxChar = ' ';
        int maxCount = 0;
        if (Pattern.compile(pattern).matcher(line).find()) {
            int[] charCount = new int[Character.MAX_VALUE + 1];
            for (int i = line.length() - 1; i >= 0; i--) {
                char ch = line.charAt(i);
                // increment this character's cnt and compare it to our max.
                if (++charCount[ch] >= maxCount && Pattern.matches(pattern, String.valueOf(ch))) {
                    maxCount = charCount[ch];
                    maxChar = ch;
                }
            }
        }
        return maxChar;
    }


    /**
     * @param field String to check
     * @param str   List of strings to check
     * @return {@code true} if any {@code str} contains in {@code field}; {@code false} otherwise
     */
    public static boolean in(String field, String... str) {
        for (String value : str) {
            if (StringUtils.isNotNull(field) && StringUtils.isNotNull(value) && field.contains(value))
                return true;
        }
        return false;
    }


    /**
     * @param field String to check
     * @return {@code true} if string is not null;{@code false} otherwise
     */
    public static boolean isNotNull(String field) {
        return !isNull(field);
    }

    /**
     * @param field String to check
     * @return {@code true} if string is null; {@code false} otherwise
     */
    public static boolean isNull(String field) {
        if (field == null)
            return true;
        else
            field = field.trim();

        return (field.equalsIgnoreCase("NULL") || field.equalsIgnoreCase("") || field.isEmpty());
    }

    /**
     * @param str String to check
     * @return {@code true} if {@code str} is numerical; {@code false} otherwise
     */
    public static boolean isNumeric(String str) {
        return StringUtils.isNotNull(str) && (str.trim().matches("[-\\+]?\\d+(\\.\\d+)?") || str.trim().matches("[-\\+]?+(\\.\\d+)?") || /*match a -ve number that ends with (.) */ str.trim().matches("[-\\+]?\\d+(\\.)?"));  //match a number with optional '-' and decimal.
        //or match a number with optional '-' and start with '.'
    }

    /**
     * @param text   String to check
     * @param length the length of substring to be taken from right of {@param text}
     * @return Substring of {@code text} from right
     */
    public static String right(String text, int length) {
        if (StringUtils.isNull(text) || text.length() <= length) {
            return (text);
        } else {

            return text.substring(text.length() - length, text.length());
        }
    }

    /**
     * @param text   String to check
     * @param length the length of substring to be taken from lefy of {@param text}
     * @return Substring of {@code text} from left
     */
    public static String left(String text, int length) {
        if (StringUtils.isNull(text) || text.length() <= length) {
            return (text);
        } else {
            return text.substring(0, length);
        }

    }

    /**
     * @param str   String to repeat
     * @param count No of times ot repeat
     * @return Repeated String
     */
    public static String repeat(String str, int count) {
        String ret = "";
        for (int i = 0; i < count; i++)
            ret += str;
        return ret;
    }

    /**
     * @param cs Character to be tested
     * @return {@code true} if all Characters are whitespace
     * {@code false} otherwise
     */
    public static boolean isWhitespace(CharSequence cs) {
        if (cs == null) {
            return false;
        } else {
            int sz = cs.length();

            for (int i = 0; i < sz; ++i) {
                if (!Character.isWhitespace(cs.charAt(i))) {
                    return false;
                }
            }

            return true;
        }
    }


}
