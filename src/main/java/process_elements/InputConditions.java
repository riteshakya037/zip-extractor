package process_elements;

import constants.ComparableConstants;
import constants.Constants;
import constants.Patterns;
import utility_and_helper.ColumnWrapper;
import utility_and_helper.FileAndExtensionWrapper;
import utility_and_helper.FileUtils;
import utility_and_helper.StringUtils;
import xls_parser_elements.CSVParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Iterates through first {@link #count} no of rows or to the EOD to check various conditions to satisfy
 * and classifies files accordingly for further processing. Also sets important information regarding
 * file such as
 * {@link #delimiter}
 * {@link #fields}
 * {@link #hasHeader}
 * {@link #emailField}
 * {@link #zipField}
 * {@link #domainField}
 * Created by Ritesh Shakya on 6/7/2016.
 */
class InputConditions {
    /**
     * Regex pattern for us Zip Codes
     */
    private static final String zipCodePattern = "\\d{3,5}(-\\d{4})?";

    /**
     * Properties of email column
     *
     * @see ColumnWrapper
     */
    ColumnWrapper emailField;
    /**
     * Properties of domain column
     *
     * @see ColumnWrapper
     */
    ColumnWrapper domainField;
    /**
     * Properties of zip column
     *
     * @see ColumnWrapper
     */
    ColumnWrapper zipField;
    /**
     * List of all the columns
     *
     * @see ColumnWrapper
     */
    private ColumnWrapper[] fields;
    /**
     * Property which defines files header
     * {@code true} if file as header
     * {@code false} otherwise
     */
    boolean hasHeader = false;
    /**
     * Delimiter which separates columns
     */
    char delimiter;
    /**
     * File condition for validity
     * {@code true} if valid for Keyword Extraction
     * {@code false} otherwise
     */
    boolean isValidForKeyWordExtraction;
    /**
     * File condition for validity
     * {@code true} if valid for Zip Extraction
     * {@code false} otherwise
     */
    boolean isValidForZipExtraction;

    /**
     * Input location of file
     * Different form {@link #originalFilePath} as XLS and XLSX files have location define in {@link Constants#TEMP_CSV_LOCATION}
     *
     * @see Constants
     */
    final String inputFilePath;
    /**
     * Input location of file
     */
    final String originalFilePath;
    /**
     * Total no of rows that were processed
     * Always less than {@link #count}
     */
    private int totalRowCount = 0;

    /**
     * Total no of rows to process before coming to a conclusion
     */
    private int count = 1000;

    /**
     * Contains a list of Columns in which Zip patterns were found with their respective count
     */
    private final HashMap<ColumnWrapper, Frequency> zipCountMap = new HashMap<>();
    /**
     * Contains a list of Columns in which Email patterns were found with their respective count
     */
    private final HashMap<ColumnWrapper, Frequency> emailCountMap = new HashMap<>();
    /**
     * Contains a list of Columns in which Domain patterns were found with their respective count
     */
    private final HashMap<ColumnWrapper, Frequency> domainCountMap = new HashMap<>();

    /**
     * Iterates through first {@link #count} no of rows or to the EOD to check various conditions to satisfy
     * and classifies files accordingly for further processing
     *
     * @param file Input file wrapper
     * @throws Exception
     * @see FileAndExtensionWrapper
     */
    InputConditions(FileAndExtensionWrapper file) throws Exception {
        this.originalFilePath = file.filePath;
        if (file.extension.equals(Constants.XLS) || file.extension.equals(Constants.XLSX)) {
            inputFilePath = FileUtils.createTempOutput(file.filePath);
        } else {
            inputFilePath = file.filePath;
        }

        try (BufferedReader brTest = new BufferedReader(new FileReader(inputFilePath))) {
            String strLine;
            boolean firstLine = true;
            while ((strLine = brTest.readLine()) != null && count != 0) {
                totalRowCount++;
                if (firstLine) {
                    getFirstLineProperties(strLine);
                    firstLine = false;
                } else {
                    CSVParser csvParser = new CSVParser(delimiter);
                    String[] lineSplit = csvParser.parseLine(strLine);
                    for (int i = 0; i < lineSplit.length; i++) {
                        checkZip(lineSplit[i].replace("\"", ""), i);
                        checkEmail(lineSplit[i].replace("\"", ""), i);
                        checkDomain(lineSplit[i].replace("\"", ""), i);
                    }
                }
                count--;
            }
            setFileValidity();
        } finally {
            emailCountMap.clear();
            zipCountMap.clear();
            domainCountMap.clear();
        }

    }

    /**
     * Checks Column value if it matches {@link Patterns#WEB_URL}
     * For each match adds the Column no {@code i} to {@link #domainCountMap} also increasing counter in each recurrence
     *
     * @param word String to check
     * @param i    Column number
     */
    private void checkDomain(String word, int i) {
        if (StringUtils.isNotNull(word)
                && (
                (Patterns.DOMAIN_NAME.matcher(word).matches() && !Patterns.EMAIL_ADDRESS.matcher(word).matches())
                        || Patterns.WEB_URL.matcher(word).matches()
        )) {
            Frequency token = domainCountMap.get(fields[i]);
            if (token == null) {
                domainCountMap.put(fields[i], new Frequency());
            } else {
                token.incrementFrequency();
            }
        }
    }

    /**
     * Checks Column value if it matches {@link Patterns#EMAIL_ADDRESS}
     * For each match adds the Column no {@code i} to {@link #emailCountMap} also increasing counter in each recurrence
     *
     * @param word String to check
     * @param i    Column number
     */
    private void checkEmail(String word, int i) {
        if (StringUtils.isNotNull(word) && Patterns.EMAIL_ADDRESS.matcher(word).matches()) {
            Frequency token = emailCountMap.get(fields[i]);
            if (token == null) {
                emailCountMap.put(fields[i], new Frequency());
            } else {
                token.incrementFrequency();
            }
        }
    }

    /**
     * Checks Column value if it matches {@link #zipCodePattern} and also contains in File pointed by {@link Constants#ZIP_FILE}
     * For each match adds the Column no {@code i} to {@link #zipCountMap} also increasing counter in each recurrence
     *
     * @param word String to check
     * @param i    Column number
     */
    private void checkZip(String word, int i) {
        if (StringUtils.isNotNull(word) && StringUtils.isNumeric(word.replaceAll("-", ""))) {
            String[] zipArray = word.split("-");
            String zip = StringUtils.right(StringUtils.repeat("0", 5) + (zipArray.length == 0 ? "" : zipArray[0]), 5);
            if (word.matches(zipCodePattern) && ProcessInput.zipOutStreamMap.containsKey(zip)) {
                Frequency token = zipCountMap.get(fields[i]);
                if (token == null) {
                    zipCountMap.put(fields[i], new Frequency());
                } else {
                    token.incrementFrequency();
                }
            }
        }
    }

    /**
     * Checks validity of Input file and sets value for
     * {@link #isValidForKeyWordExtraction}
     * {@link #isValidForZipExtraction}
     * {@link #zipField}
     * {@link #emailField}
     * {@link #domainField}
     */
    private void setFileValidity() {
        int maxZipSoFar = 0;
        int maxEmailSoFar = 0;
        int maxDomainSoFar = 0;
        int countCheckForValid = totalRowCount * 5 / 10;

        for (ColumnWrapper key : zipCountMap.keySet()) {
            if (maxZipSoFar < zipCountMap.get(key).getFrequency()) {
                maxZipSoFar = zipCountMap.get(key).getFrequency();
                if (maxZipSoFar > countCheckForValid) {
                    isValidForZipExtraction = true;
                    zipField = key;
                } else {
                    isValidForZipExtraction = false;
                }
            }
        }
        for (ColumnWrapper key : emailCountMap.keySet()) {
            if (maxEmailSoFar < emailCountMap.get(key).getFrequency()) {
                maxEmailSoFar = emailCountMap.get(key).getFrequency();
                if (maxEmailSoFar > countCheckForValid) {
                    emailField = key;
                    isValidForKeyWordExtraction = true;
                } else {
                    isValidForZipExtraction = false;
                }
            }
        }
        for (ColumnWrapper key : domainCountMap.keySet()) {
            if (maxDomainSoFar < domainCountMap.get(key).getFrequency()) {
                maxDomainSoFar = domainCountMap.get(key).getFrequency();
                if (maxDomainSoFar > countCheckForValid) {
                    domainField = key;
                }
            }
        }
        if (emailCountMap.size() == 0 || zipCountMap.size() == 0) {
            isValidForZipExtraction = false;
        }
    }

    /**
     * Sets essential values of the file such as
     * {@link #delimiter}
     * {@link #fields}
     * {@link #hasHeader}
     * Also if {@link #hasHeader} is {@code true} checks for certain additional values {@link #zipField},{@link #emailField}
     * If {@link #hasHeader} is {@code false} sets up pseudo {@link #fields} with values as field#
     *
     * @param firstLine First line of File
     */
    private void getFirstLineProperties(String firstLine) {
        if (StringUtils.in(firstLine, "name", "zip", "email", "address")) {
            hasHeader = true;
        }
        delimiter = StringUtils.getMaxOccurringChar(firstLine);
        CSVParser csvParser = new CSVParser(delimiter);
        String[] fieldNames = csvParser.parseLine(firstLine);
        String[] tempFieldName = new String[fieldNames.length];
        ColumnWrapper[] pseudoWrapper = new ColumnWrapper[fieldNames.length];
        fields = new ColumnWrapper[fieldNames.length];

        for (int i = 0; i < fieldNames.length; i++) {

            fieldNames[i] = fieldNames[i].replaceAll("[^\\w]", "").toLowerCase();
            fields[i] = new ColumnWrapper(fieldNames[i], i);

            tempFieldName[i] = "field" + String.valueOf(i);
            pseudoWrapper[i] = new ColumnWrapper(tempFieldName[i], i);

            if (fieldNames[i].contains(ComparableConstants.ZIP)) {
                zipField = new ColumnWrapper(fieldNames[i], i);
            }
            if (fieldNames[i].contains(ComparableConstants.EMAIL)) {
                emailField = new ColumnWrapper(fieldNames[i], i);
            }
            if (StringUtils.isNumeric(fieldNames[i])) {
                hasHeader = false;
            }
        }
        if (!hasHeader) {
            fields = pseudoWrapper;
        }
    }

    @Override
    public String toString() {
        return "InputConditions{" +
                "emailField=" + emailField.toString() +
                ", zipField=" + zipField.toString() +
                ", fields=" + Arrays.toString(fields) +
                ", hasHeader=" + hasHeader +
                ", delimiter='" + delimiter + '\'' +
                ", isValidForZipExtraction=" + isValidForZipExtraction +
                ", inputFilePath='" + inputFilePath + '\'' +
                ", originalFilePath='" + originalFilePath + '\'' +
                '}';
    }

    /**
     * Counter class to check count of each column {@link ColumnWrapper} used in {@link #domainCountMap},{@link #zipCountMap},{@link #emailCountMap}
     */
    private class Frequency {

        /**
         * Count of each occurrence
         */
        private int count;

        private Frequency() {
            this.count = 1;
        }

        /**
         * @return Count of each Key
         */
        private int getFrequency() {
            return this.count;
        }

        /**
         * Increase count of {@link #count} if called
         */
        private void incrementFrequency() {
            this.count++;
        }

        @Override
        public String toString() {
            return String.valueOf(count);
        }
    }
}
