package process_elements;

import constants.Constants;
import utility_and_helper.SyncData;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Ritesh Shakya on 6/14/2016.
 */
class ZipCountWriter implements Runnable {
    private final String zip;
    private final int count;

    public ZipCountWriter(String zip, int count) {
        this.zip = zip;
        this.count = count;
    }

    public static void writeFile(String outputLocation) {
        String outputFile = outputLocation + Constants.ZIP_FILES_LOCATION + Constants.ZIP_COUNT;
        try (PrintWriter out = new PrintWriter(new FileWriter(outputFile))) {
            out.write("Zip,Count" + System.lineSeparator());
            for (String zipSets : SyncData.zipCount) {
                out.write(zipSets + System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace(System.out);
        } finally {
            SyncData.zipCount.clear();
        }

    }

    @Override
    public void run() {
        try {
            SyncData.getInstance().appendToHash(zip, count);
        } catch (IOException e) {
            e.printStackTrace(System.out);
        }

    }

}
