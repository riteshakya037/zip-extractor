package process_elements;

import constants.Constants;
import ui_elements.App;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.StringJoiner;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

/**
 * Singleton class for each keyword found in input.
 * Each keyword is a assigned a singleton with a {@link OutputStream} initiated
 * on each row with the keyword a new row is written to {@link #out}
 * Created by Ritesh Shakya on 6/21/2016.
 */
public class KeyWordSync {
    private static HashMap<String, KeyWordSync> instanceMap = new HashMap<>();
    private OutputStream out = null;
    private String key;

    /**
     * @param key Keyword for which singleton is created and {@link OutputStream}  is created
     * @throws IOException
     */
    private KeyWordSync(String key) throws IOException {
        String outputDirectory = App.getInputDirectory() + Constants.TEMP_ZIP_LOCATION + key + ".csv";
        out = Files.newOutputStream(Paths.get(outputDirectory), CREATE, APPEND);
        this.key = key;
    }

    /**
     * @param key Creates a Singleton for each {@link #key} and saves all instances to a map {@link #instanceMap}
     * @throws IOException
     */
    public static KeyWordSync getInstance(String key) throws IOException {
        synchronized (KeyWordSync.class) {
            if (!instanceMap.containsKey(key)) {
                instanceMap.put(key, new KeyWordSync(key));
            }
        }
        return instanceMap.get(key);
    }

    /**
     * @param joiner Output row received from {@link KeywordWriter} after matching with Keyword from {@link KeywordWriter}
     * @throws IOException
     */
    public void write(StringJoiner joiner) throws IOException {
        synchronized (instanceMap.get(key)) {
            out.write((joiner.toString() + System.lineSeparator()).getBytes());
        }
    }
}
