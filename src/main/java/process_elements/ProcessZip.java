package process_elements;

import constants.Constants;
import utility_and_helper.FileAndExtensionWrapper;
import utility_and_helper.FileUtils;
import ui_elements.App;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static ui_elements.App.statusText;

/**
 * Created by Ritesh Shakya on 6/2/2016.
 */
public class ProcessZip extends SwingWorker<Void, Void> {
    private final ExecutorService pool;
    private final List<FileAndExtensionWrapper> fileAndExtensionWrappers;
    private final long start;
    private long startThis;

    public ProcessZip(List<FileAndExtensionWrapper> fileAndExtensionWrappers, long start) {
        this.fileAndExtensionWrappers = fileAndExtensionWrappers;
        this.start = start;
        pool = Executors.newFixedThreadPool(Constants.THREAD_POOL_SIZE);

    }

    protected Void doInBackground() throws Exception {
        startThis = System.currentTimeMillis();
        fileAndExtensionWrappers.stream().filter(file -> file.extension.equalsIgnoreCase(Constants.CSV)).forEach(file -> {
            try {
                pool.execute(new ProcessZipFiles(file.filePath));
            } catch (Exception ex) {
                ex.printStackTrace(System.out);
            }
        });
        pool.shutdown();
        while (!pool.isTerminated()) {
            statusText(null, ProcessZipFiles.completed * 100 / fileAndExtensionWrappers.size() + "% Done");
            Thread.sleep(100);
        }
        ZipCountWriter.writeFile(FileUtils.getParent(fileAndExtensionWrappers.get(0).filePath));
        return null;
    }

    @Override
    protected void done() {
        long end = System.currentTimeMillis();
        ProcessZipFiles.completed = 0;
        App.reEnableUI();
        NumberFormat formatter = new DecimalFormat("#0.00");
        System.out.println("Zip Processing completed in " + formatter.format((end - startThis) / 1000d) + " seconds");
        statusText("Extraction Completed in " + formatter.format((end - start) / 1000d) + " seconds" + (ProcessInput.success ? "" : " with failure."), "");
        ProcessBuilder pb = new ProcessBuilder("java", "-classpath", App.getJarPath(), "ui_elements.App", String.valueOf(end - start));
        try {
            Desktop.getDesktop().open(new File(App.getInputDirectory()));
            pb.start();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        } finally {
            System.exit(0);
        }
    }

}
