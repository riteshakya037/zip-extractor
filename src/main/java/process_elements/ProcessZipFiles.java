package process_elements;

import constants.Constants;
import utility_and_helper.FileUtils;
import utility_and_helper.StringUtils;
import ui_elements.App;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.stream.Stream;

/**
 * Created by Ritesh Shakya on 6/2/2016.
 */
public class ProcessZipFiles implements Runnable {
    private final String filePath;
    private final HashMap<String, String> emailNoDup = new HashMap<>();
    public static int completed = 0;

    public ProcessZipFiles(String filePath) throws Exception {
        this.filePath = filePath;
    }

    public void run() {
        String outputFolder = App.getInputDirectory() + Constants.ZIP_FILES_LOCATION;
        String outputFile = outputFolder + FileUtils.getFile(filePath);
        Path file = Paths.get(filePath);
        if (!(new File(outputFolder)).exists()) {
            (new File(outputFolder)).mkdir();
        }
        try (Stream<String> lines = Files.lines(file, StandardCharsets.UTF_8)) {
            for (String line : (Iterable<String>) lines::iterator) {
                if (StringUtils.isNotNull(line)) {
                    emailNoDup.put(line.split(",")[0], line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        try (BufferedWriter out = new BufferedWriter(new FileWriter(outputFile), 32768)) {
            for (String key : emailNoDup.keySet()) {
                out.write(emailNoDup.get(key) + System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            new ZipCountWriter(FileUtils.getFile(filePath).replace(".csv", ""), emailNoDup.size()).run();
            emailNoDup.clear();
        }
    }

}
