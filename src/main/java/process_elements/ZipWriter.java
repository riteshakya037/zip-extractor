package process_elements;

import constants.Constants;
import utility_and_helper.StringUtils;
import xls_parser_elements.CSVParser;

import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.StringJoiner;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;
import static process_elements.ProcessInput.zipOutStreamMap;

/**
 * Created by Ritesh Shakya on 6/7/2016.
 */
class ZipWriter implements Runnable {
    private static final String zipCodePattern = "\\d{3,5}(-\\d{4})?";
    private String zip;
    private final String strLine;
    private final InputConditions inputProperty;
    private final String outputPath;
    static long linesDone = 0;

    ZipWriter(String strLine, InputConditions inputProperty, String outputPath) throws Exception {
        this.strLine = strLine;
        this.inputProperty = inputProperty;
        this.outputPath = outputPath;
    }

    public void run() {
        CSVParser csvParser = new CSVParser(inputProperty.delimiter);
        String[] lineSplit = csvParser.parseLine(strLine);

        StringJoiner joiner = new StringJoiner(",");
        if (lineSplit.length != 0) {
            if (lineSplit.length > inputProperty.zipField.position && lineSplit.length > inputProperty.emailField.position) {
                String[] zipArray = lineSplit[inputProperty.zipField.position].replace("\"", "").split("-");
                zip = StringUtils.right(StringUtils.repeat("0", 5) + (zipArray.length == 0 ? "" : zipArray[0]), 5);
                joiner.add(lineSplit[inputProperty.emailField.position].replace("\"", "").trim()).add(lineSplit[inputProperty.zipField.position].replace("\"", "").trim());
                for (int i = 0; i < lineSplit.length; i++) {
                    if (!(i == inputProperty.emailField.position || i == inputProperty.zipField.position) && StringUtils.isNotNull(lineSplit[i])) {
                        if (lineSplit[i].contains(String.valueOf(inputProperty.delimiter))) {
                            joiner.add("\"" + lineSplit[i].replace("\"", "").trim() + "\"");
                        } else {
                            joiner.add(lineSplit[i].replace("\"", "").trim());
                        }
                    }
                }
                try {
                    if (zipOutStreamMap.containsKey(zip) && lineSplit[inputProperty.zipField.position].replace("\"", "").matches(zipCodePattern)) {
                        write(outputPath, joiner, true);
                    } else {
                        write(outputPath, joiner, false);
                    }
                    Thread.currentThread().interrupt();
                } catch (Exception e) {
                    e.printStackTrace(System.out);
                }
            } else if (lineSplit.length > inputProperty.emailField.position) {
                joiner.add(lineSplit[inputProperty.emailField.position].replace("\"", "")).add("");
                for (int i = 0; i < lineSplit.length; i++) {
                    if (!(i == inputProperty.emailField.position || i == inputProperty.zipField.position) && StringUtils.isNotNull(lineSplit[i])) {
                        if (lineSplit[i].contains(String.valueOf(inputProperty.delimiter))) {
                            joiner.add("\"" + lineSplit[i].replace("\"", "") + "\"");
                        } else {
                            joiner.add(lineSplit[i].replace("\"", ""));
                        }
                    }
                }
                try {
                    write(outputPath, joiner, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private synchronized void write(String file, StringJoiner joiner, boolean valid) throws Exception {
        OutputStream out = zipOutStreamMap.get(valid ? zip : Constants.INVALID_ZIP_FILE);
        if (out == null) {
            out = Files.newOutputStream(Paths.get(file + (valid ? zip : Constants.INVALID_ZIP_FILE) + ".csv"), CREATE, APPEND);
            zipOutStreamMap.put(zip, out);
        }
        out.write((joiner.toString() + System.getProperty("line.separator")).getBytes());
        out.flush();
        linesDone++;
    }
}
