package process_elements;

import constants.Constants;
import ui_elements.App;
import utility_and_helper.FileAndExtensionWrapper;
import utility_and_helper.FileUtils;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

import static ui_elements.App.isZipProcessSelected;
import static ui_elements.App.statusText;

/**
 * Extracts all zips and keywords by iterating through each row of Input in a ThreadPool.
 * Also checks each file for necessary conditions.
 * Created by Ritesh Shakya on 6/7/2016.
 */
public class ProcessInputThread {
    /**
     * Asynchronous Thread pool to run each row for processing
     */
    private final ExecutorService pool;
    /**
     * Output Folder for temporary files
     *
     * @see Constants#TEMP_ZIP_LOCATION
     */
    private String outputPath;
    /**
     * No of Lines in a files. Add to this as processing starts for each row.
     */
    private long lineCount = 0;
    /**
     * Class which stores importation properties of a file
     *
     * @see InputConditions
     */
    private InputConditions inputProperty;
    /**
     * Time at which processing for this file started
     */
    private long start;

    /**
     * Initializes Thread pool with a size of {@link Constants#THREAD_POOL_SIZE}
     */
    public ProcessInputThread() {
        pool = Executors.newFixedThreadPool(Constants.THREAD_POOL_SIZE);
    }

    /**
     * Checks the file for validity and starts processing based on it
     *
     * @param filePath Input file wrapper
     * @throws Exception
     * @see FileAndExtensionWrapper
     * @see InputConditions
     */
    public void buildSourceTapsAndPipes(FileAndExtensionWrapper filePath) throws Exception {
        outputPath = FileUtils.getFolder(filePath.filePath) + Constants.TEMP_ZIP_LOCATION;
        start = System.currentTimeMillis();
        inputProperty = new InputConditions(filePath);
        if (inputProperty.isValidForZipExtraction) {
            startExtraction(true);
            FileUtils.moveFile(inputProperty.originalFilePath, Constants.VALID_FILE);
        } else if (inputProperty.isValidForKeyWordExtraction) {
            startExtraction(false);
            FileUtils.moveFile(inputProperty.originalFilePath, Constants.VALID_KEYWORD_FILE);
        } else {
            FileUtils.moveFile(inputProperty.originalFilePath, Constants.INVALID_FILE);
        }
        NumberFormat formatter = new DecimalFormat("#0.00000");

        System.out.println("Finished \"" + FileUtils.getFile(filePath.filePath) + "\" in " + formatter.format((System.currentTimeMillis() - start) / 1000d) + " seconds");
        lineCount = 0;
        ZipWriter.linesDone = 0;
    }

    /**
     * Iterates through all the lines in Input File
     * Also sets done % completion status on {@link App#statusText(String, String)}
     *
     * @param zipExtraction {@code true} Valid for zip extraction
     * @throws InterruptedException
     * @see ZipWriter
     * @see KeywordWriter
     */
    private void startExtraction(boolean zipExtraction) throws InterruptedException {
        try {
            Path file = Paths.get(inputProperty.inputFilePath);
            boolean once = inputProperty.hasHeader;
            if (!(new File(outputPath)).exists()) {
                (new File(outputPath)).mkdir();
            }
            Stream<String> lines = Files.lines(file, StandardCharsets.UTF_8);
            for (String line : (Iterable<String>) lines::iterator) {
                lineCount++;
                synchronized (this) {
                    if (once) {
                        once = false;
                    } else {
                        if (isZipProcessSelected() && zipExtraction) {
                            pool.execute(new ZipWriter(line, inputProperty, outputPath));
                        }
                        pool.execute(new KeywordWriter(line, inputProperty));
                    }
                }
            }
            lines.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        pool.shutdown();
        while (!pool.isTerminated()) {
            statusText(null, ZipWriter.linesDone * 100 / lineCount + "% Done");
            Thread.sleep(100);
        }
    }


}
