package process_elements;

import utility_and_helper.KeywordMatcher;
import utility_and_helper.StringUtils;
import xls_parser_elements.CSVParser;

import java.util.StringJoiner;

/**
 * Created by Ritesh Shakya on 6/21/2016.
 * rearranges the column with email and zip leading and the other columns following
 * and sends the entire row {@link KeywordMatcher}
 */
public class KeywordWriter implements Runnable {
    private final String strLine;
    private final InputConditions inputProperty;
    private final int emailPosition;
    private final int zipPosition;

    /**
     * @param strLine       Row value of input text
     * @param inputProperty Input Properties of file such as zip_column, email_column, hasHeader
     */
    public KeywordWriter(String strLine, InputConditions inputProperty) {
        this.strLine = strLine;
        this.inputProperty = inputProperty;
        this.emailPosition = inputProperty.emailField.position;
        this.zipPosition = inputProperty.zipField == null ? 99 : inputProperty.zipField.position;
    }

    /**
     * Rearranges the row to keep email and zip at the start and the rest following.
     * Also calls {@link KeywordMatcher} for each row to check if it contains any keyword
     */
    @Override
    public void run() {
        CSVParser csvParser = new CSVParser(inputProperty.delimiter);
        String[] lineSplit = csvParser.parseLine(strLine);
        StringJoiner joiner = new StringJoiner(",");
        if (lineSplit.length > emailPosition) { //check if the row is complete
            if (lineSplit.length != 0) {
                joiner.add(lineSplit[emailPosition].replace("\"", "").trim()) //add email to first position of output
                        .add(lineSplit.length > zipPosition ? lineSplit[zipPosition].replace("\"", "").trim() : ""); //add zip to second position if exists
                for (int i = 0; i < lineSplit.length; i++) {
                    if (notEmailOrZipField(i) && StringUtils.isNotNull(lineSplit[i])) { //don't have to add email and zip again in output
                        if (lineSplit[i].contains(String.valueOf(inputProperty.delimiter))) {
                            //Wrap with quotes if text contains "," (delimiter) inside
                            joiner.add("\"" + lineSplit[i].replace("\"", "").trim() + "\"");
                        } else {
                            joiner.add(lineSplit[i].replace("\"", "").trim());
                        }
                    }
                }
                if (inputProperty.domainField == null) {
                    // file may not have a domain to be correct
                    KeywordMatcher.matches(lineSplit[emailPosition], joiner);
                } else {
                    KeywordMatcher.matches(lineSplit[inputProperty.domainField.position], lineSplit[emailPosition], joiner);
                }

            }
        }
    }

    /**
     * @param i column number for the row
     * @return whether zip or email row
     */
    private boolean notEmailOrZipField(int i) {
        return !(i == emailPosition || i == zipPosition);
    }
}
