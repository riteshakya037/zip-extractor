package process_elements;

import constants.Constants;
import file_elements.FileListGetter;
import file_elements.FileListGetterZip;
import ui_elements.App;
import utility_and_helper.FileAndExtensionWrapper;
import utility_and_helper.FileUtils;
import xls_parser_elements.ExcelParser;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ui_elements.App.getInputDirectory;
import static ui_elements.App.statusText;

/**
 * {@author Ritesh Shakya}
 */
public class ProcessInput extends SwingWorker<Void, FileAndExtensionWrapper> {
    /**
     * A map of all zip code in File {@link Constants#ZIP_FILE} along with their respective {@link OutputStream}
     *
     * @see Constants#TEMP_ZIP_LOCATION
     * @see Constants#ZIP_FILE
     */
    public static final HashMap<String, OutputStream> zipOutStreamMap = new HashMap<>();
    /**
     * Time at which processing started
     */
    private long start;
    /**
     * List of all the files found by {@link FileListGetter}
     */
    private final List<FileAndExtensionWrapper> fileAndExtensionWrappers;
    /**
     * Flag to see if all files were extracted successfully
     * If {@code false} error in one or more file
     */
    public static boolean success = true;
    /**
     * Current working file
     */
    private static FileAndExtensionWrapper currentFile;


    /**
     * Initializes {@link #zipOutStreamMap} by reading File {@link Constants#ZIP_FILE}
     * and setting their respective {@link OutputStream} to {@code null}
     *
     * @param fileAndExtensionWrappers List of all the files found by {@link FileListGetter}
     */
    public ProcessInput(List<FileAndExtensionWrapper> fileAndExtensionWrappers) {
        this.fileAndExtensionWrappers = fileAndExtensionWrappers;
        try {
            BufferedReader brTest = new BufferedReader(new InputStreamReader(ProcessInput.class.getResourceAsStream(Constants.ZIP_FILE)));
            String strLine;
            while ((strLine = brTest.readLine()) != null) {
                ProcessInput.zipOutStreamMap.put(strLine.split(",")[0], null);
            }
            brTest.close();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    /**
     * Goes through all the files converting them to CSV if necessary and processing them
     * Iterates through all the files even if one fails.
     * On chance that even on fails {@link #success} is set to {@code false}
     *
     * @throws Exception
     * @see ProcessInputThread
     * @see ExcelParser
     */
    protected Void doInBackground() throws Exception {
        start = System.currentTimeMillis();
        for (FileAndExtensionWrapper file : fileAndExtensionWrappers) {
            try {

                int currentFileIndex = fileAndExtensionWrappers.indexOf(file) + 1;
                statusText("Working on " + currentFileIndex + "/" + fileAndExtensionWrappers.size() + " - " + FileUtils.getFile(file.filePath), "");

                if (file.extension.equalsIgnoreCase(Constants.CSV) || file.extension.equalsIgnoreCase(Constants.TXT)) {
                    publish(file);
                    statusText(null, "Initializing...");
                    (new ProcessInputThread()).buildSourceTapsAndPipes(file);
                } else if (file.extension.equalsIgnoreCase(Constants.XLS) || file.extension.equalsIgnoreCase(Constants.XLSX)) {
                    statusText(null, "Converting to CSV...");
                    ExcelParser parser = new ExcelParser();
                    publish(file);
                    parser.generateCSV(file);
                    statusText(null, "Initializing...");
                    (new ProcessInputThread()).buildSourceTapsAndPipes(file);
                }
            } catch (Exception e) {
                e.printStackTrace(System.out);
                System.out.println("Failed File: " + currentFile.filePath);
                FileUtils.moveFile(currentFile.filePath, Constants.FAILED_FILE);
                success = false;
            }
        }
        closeOutputStream();
        return null;
    }


    /**
     * Closes all {@link OutputStream} initialized in {@link #zipOutStreamMap}
     * to prevent memory leaks. Also sets percentage completion on {@link App#statusText(String, String)}
     *
     * @throws InterruptedException
     */
    private void closeOutputStream() throws InterruptedException {
        long cleaningStart = System.currentTimeMillis();
        statusText("Cleaning Up...", "Initializing...");
        int totalZip = zipOutStreamMap.size();
        final int[] completed = {0};

        for (Map.Entry<String, OutputStream> out : zipOutStreamMap.entrySet()) {
            try {
                if (out.getValue() != null) {
                    out.getValue().close();
                    out.setValue(null);
                }
                completed[0]++;
                statusText(null, completed[0] * 100 / totalZip + "% Done");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        zipOutStreamMap.clear();
        NumberFormat formatter = new DecimalFormat("#0.00000");
        System.out.println("Cleaning Finished in " + formatter.format((System.currentTimeMillis() - cleaningStart) / 1000d) + " seconds");
        Thread.sleep(1000);
    }

    /**
     * Starts {@link FileListGetterZip} to start Zip Processing to get unique values
     * as up-to now duplicates may exist
     */
    @Override
    protected void done() {
        NumberFormat formatter = new DecimalFormat("#0.00");
        System.out.println("Execution time is " + formatter.format((System.currentTimeMillis() - start) / 1000d) + " seconds");
        (new FileListGetterZip(getInputDirectory() + Constants.TEMP_ZIP_LOCATION, start)).execute();
        statusText("Processing Zip Files", "");
    }

    /**
     * Used to set the current file of execution
     *
     * @param chunks Every time {@link #publish(Object[])} is called {@code Object} is added to this list
     */
    @Override
    protected void process(List<FileAndExtensionWrapper> chunks) {
        currentFile = chunks.get(chunks.size() - 1);
    }
}
