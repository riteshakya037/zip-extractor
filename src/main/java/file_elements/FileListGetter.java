package file_elements;

import constants.Constants;
import process_elements.ProcessInput;
import utility_and_helper.FileAndExtensionWrapper;
import utility_and_helper.FileUtils;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static ui_elements.App.reEnableUI;
import static ui_elements.App.statusText;

/**
 * Gets the list of all Valid files types in Input Location
 * Created by Ritesh Shakya on 6/2/2016.
 */
public class FileListGetter extends SwingWorker<List<FileAndExtensionWrapper>, Void> {

    /**
     * Folder into which to look for files
     */
    private final String inputLocation;

    /**
     * @param inputLocation Folder into which to look for files
     */
    public FileListGetter(String inputLocation) {
        this.inputLocation = inputLocation;
    }

    /**
     * Deletes {@link Constants#TEMP_ZIP_LOCATION} if both it and {@link Constants#ZIP_FILES_LOCATION} exists
     * also moves {@link Constants#ZIP_FILES_LOCATION} to {@link Constants#TEMP_ZIP_LOCATION}
     *
     * @return List of all the files using wrapper
     * @see FileAndExtensionWrapper
     */
    protected List<FileAndExtensionWrapper> doInBackground() {
        File doneFolder = new File(inputLocation + Constants.ZIP_FILES_LOCATION);
        File tempFolder = new File(inputLocation + Constants.TEMP_ZIP_LOCATION);
        if (tempFolder.exists() && doneFolder.exists()) {
            statusText("Deleting Temp Folder", "");
            FileUtils.deleteFolder(tempFolder.getPath());
        }
        if (doneFolder.exists()) {
            FileUtils.moveFolder(doneFolder.getPath(), tempFolder.getPath());
        }
        List<FileAndExtensionWrapper> fileHash = new ArrayList<>();
        File[] files = new File(inputLocation).listFiles((dir, name) -> name.toLowerCase().endsWith(Constants.CSV) || name.toLowerCase().endsWith(Constants.XLS) || name.toLowerCase().endsWith(Constants.XLSX));
        if (files != null) {
            for (File file : files) {
                fileHash.add(new FileAndExtensionWrapper(file));
            }
        }
        return fileHash;
    }

    /**
     * Starts Processing on each files inside {@link #inputLocation}
     */
    @Override
    protected void done() {
        try {
            if (get().size() != 0) {
                new ProcessInput(get()).execute();
            } else {
                reEnableUI();
                statusText("No Files Found", "");
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace(System.out);
        }
    }

}


