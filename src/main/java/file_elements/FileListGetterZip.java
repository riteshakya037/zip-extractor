package file_elements;

import constants.Constants;
import process_elements.ProcessZip;
import utility_and_helper.FileAndExtensionWrapper;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static ui_elements.App.reEnableUI;
import static ui_elements.App.statusText;

/**
 * Gets the list of all Valid files types in {@link Constants#TEMP_ZIP_LOCATION}
 * Created by Ritesh Shakya on 6/2/2016.
 */
public class FileListGetterZip extends SwingWorker<List<FileAndExtensionWrapper>, Void> {

    /**
     * Folder into which to look for files
     */
    private final String inputLocation;
    /**
     * Time in which processing started
     */
    private final long start;

    /**
     * @param inputLocation Folder into which to look for files
     * @param start         Time in which processing started
     */
    public FileListGetterZip(String inputLocation, long start) {
        this.inputLocation = inputLocation;
        this.start = start;
    }

    /**
     * @return List of all the files using wrapper
     * @see FileAndExtensionWrapper
     */
    protected List<FileAndExtensionWrapper> doInBackground() throws Exception {
        List<FileAndExtensionWrapper> fileHash = new ArrayList<>();
        File[] files = new File(inputLocation).listFiles((dir, name) -> name.endsWith(Constants.CSV) && !name.equalsIgnoreCase(Constants.ZIP_COUNT));
        if (files != null) {
            for (File file : files) {
                fileHash.add(new FileAndExtensionWrapper(file));
            }
        }
        return fileHash;
    }

    /**
     * Starts Processing on each files inside {@link #inputLocation}
     */
    @Override
    protected void done() {
        try {
            if (get().size() != 0) {
                new ProcessZip(get(), start).execute();
            } else {
                reEnableUI();
                statusText("", "No Zip Files Found");
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }


}


