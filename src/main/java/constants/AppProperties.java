package constants;

/**
 * Enum Key for Program Properties to store
 * Created by Ritesh Shakya on 6/19/2016.
 */
public enum AppProperties {
    /**
     * Input Location of Extraction
     */
    InputLocation("inputLocation");

    private final String value;

    /**
     * @param value Property Name for key
     */
    AppProperties(String value) {
        this.value = value;
    }

    /**
     * @return key used to store property
     */
    public String getValue() {
        return value;
    }
}
