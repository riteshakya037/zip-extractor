package constants;

/**
 * Constant class to check value for column
 * Created by Ritesh Shakya on 6/6/2016.
 */
public class ComparableConstants {
    public static final String ZIP = "zip";
    public static final String EMAIL = "email";
}
