package constants;

import java.io.File;

/**
 * Commonly used regular Constants.
 * Created by Ritesh Shakya on 6/6/2016.
 */
public class Constants {

    /**
     * Extension of files that is processed by Program
     */
    public static final String CSV = ".csv";

    /**
     * Extension of files that is processed by Program
     */
    public static final String XLS = ".xls";

    /**
     * Extension of files that is processed by Program
     */
    public static final String XLSX = ".xlsx";

    /**
     * Extension of files that is processed by Program
     */
    public static final String TXT = ".txt";


    /**
     * Folder Name for Files that are found with zip
     */
    public static final String VALID_FILE = File.separator + "Zip Files" + File.separator;
    /**
     * Folder Name for temporary files that are converted from XLS or XLXS
     */
    public static final String TEMP_CSV_LOCATION = File.separator + "temp_csv" + File.separator;
    /**
     * Folder Name for Files that are not found with zip
     */
    public static final String INVALID_FILE = File.separator + "No Zip Files" + File.separator;
    /**
     * Folder Name for final Output Zip files
     */
    public static final String ZIP_FILES_LOCATION = File.separator + "Done" + File.separator;
    /**
     * Folder Name for temporary Output Zip files in which duplicates exists
     */
    public static final String TEMP_ZIP_LOCATION = File.separator + "temp_zip" + File.separator;
    /**
     * Folder Name for Files that have failed
     */
    public static final String FAILED_FILE = File.separator + "Failed" + File.separator;
    /**
     * Fodler Name for files that dont have zip but are valid for Keyword Extraction
     */
    public static final String VALID_KEYWORD_FILE = File.separator + "Keyword File" + File.separator;

    /**
     * File Name of Count file
     */
    public static final String ZIP_COUNT = "~Count.csv";
    /**
     * No of active threads in a pool
     */
    public static final int THREAD_POOL_SIZE = 50;

    /**
     * Location to start folder selector
     *
     * @see ui_elements.App
     */
    public static final String INPUT_SELECTOR_LOCATION = ".\\src\\test\\resources\\Input Files";
    /**
     * Folder name for rows that are form valid file but with invalid zip
     */
    public static final String INVALID_ZIP_FILE = "Invalid_Zip_From_Valid_File.csv";
    /**
     * Internal location of list of Zip File
     */
    public static final String ZIP_FILE = "/csv_files/us_postal_codes.csv";
    /**
     * Folder Name for Log Files
     */
    public static final String LOG_DIRECTORY = File.separator + "Logs" + File.separator;
}
