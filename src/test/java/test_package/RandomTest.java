package test_package;

import constants.Patterns;
import org.junit.Test;
import utility_and_helper.StringUtils;

/**
 * Created by Ritesh Shakya on 6/3/2016.
 */
public class RandomTest {
    @Test
    public void test() {
        String delimiter = ";";
        String line = "foo;bar;c,qual=\"baz;blurb\";d,junk=\"quux;syzygy\"";
        String[] tokens = line.split(delimiter + "(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
        for (String t : tokens) {
            System.out.println("> " + t);
        }
    }

    @Test
    public void testZip() {
        String zip = "-";
        System.out.println(StringUtils.right(StringUtils.repeat("0", 5) + zip.replace("\"", "").split("-")[0], 5));
    }


    @Test
    public void DomainTest() {
        String word = "www.google.com";
        if (StringUtils.isNotNull(word)
                && (
                (Patterns.DOMAIN_NAME.matcher(word).matches() && !Patterns.EMAIL_ADDRESS.matcher(word).matches())
                        || Patterns.WEB_URL.matcher(word).matches()
        )) {
            System.out.println("accepted");
        }
    }
}
