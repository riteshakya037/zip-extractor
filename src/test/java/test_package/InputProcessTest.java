package test_package;

import constants.Constants;
import utility_and_helper.FileAndExtensionWrapper;
import org.junit.Test;
import process_elements.ProcessInput;
import process_elements.ProcessInputThread;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

/**
 * Created by Ritesh Shakya on 6/2/2016.
 */
public class InputProcessTest {

    @Test
    public void testBuilder() throws Exception {
        try {
            BufferedReader brTest = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(Constants.ZIP_FILE)));
            String strLine;
            while ((strLine = brTest.readLine()) != null) {
                ProcessInput.zipOutStreamMap.put(strLine.split(",")[0], null);
            }
            brTest.close();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        (new ProcessInputThread()).buildSourceTapsAndPipes(new FileAndExtensionWrapper(new File("E:\\Test2\\clean_157142-192565_Apr_04_2014_04_59_15.csv")));
    }

}
